package com.gmail.val59000mc;

import com.gmail.val59000mc.players.PlayerManager;
import com.gmail.val59000mc.scoreboard.ScoreboardManager;
import com.gmail.val59000mc.scoreboard.ScoreboardType;
import com.gmail.val59000mc.game.GameManager;
import com.gmail.val59000mc.utils.TimeUtils;
import com.gmail.val59000mc.configuration.MainConfig;
import com.gmail.val59000mc.players.UhcPlayer; 
import com.gmail.val59000mc.languages.Lang;
<<<<<<< Updated upstream:src/main/java/com/gmail/val59000mc/placeholderapi.java

=======
import com.gmail.val59000mc.scoreboard.ScoreboardLayout;
>>>>>>> Stashed changes:src/main/java/com/gmail/val59000mc/Placeholderapi.java
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
public class Placeholderapi extends PlaceholderExpansion {
    private final UhcCore plugin; 

    public Placeholderapi(UhcCore plugin) {
        this.plugin = plugin; 
    }

    public String getAuthor() {
        return "zerodind"; 
    }

    public String getIdentifier(){
        return "uhccore"; 
    }

    public String getVersion(){
        return "1.20.5"; 
    }


    public boolean persist(){
        return true; 
    }

    /**
	 * This override registers placeholders 
	 */

    // %uhccore_playerAlive%

    public String onRequest(OfflinePlayer player, String params) {
      GameManager gm = GameManager.getGameManager();
      MainConfig cfg = gm.getConfig();

<<<<<<< Updated upstream:src/main/java/com/gmail/val59000mc/placeholderapi.java
      
      
      PlayerManager manager = gm.getPlayerManager(); 
      Player bukkitPlayer = player.getPlayer(); 
      UhcPlayer uhcPlayer = manager.getUhcPlayer(bukkitPlayer); 


      if (params.equalsIgnoreCase("playerAlive")) {
        return String.valueOf(gm.getPlayerManager().getAllPlayingPlayers().size()); 
      }

      if (params.equalsIgnoreCase("teamAlive")){
        long pvp = cfg.get(MainConfig.TIME_BEFORE_PVP) - gm.getElapsedTime();
      
        if (pvp < 0){
          return "-";
        }else {
          return TimeUtils.getFormattedTime(pvp);
        }
      
      }

      if (params.equalsIgnoreCase("time")) {
        return TimeUtils.getFormattedTime(gm.getElapsedTime()); 
      }

      if (params.equalsIgnoreCase("deathmatch")) {
        return gm.getFormattedRemainingTime(); 
      }

      if (params.equalsIgnoreCase("xCoordinate")) {
        return (int) bukkitPlayer.getLocation().getX() + ""; 
      }

      if (params.equalsIgnoreCase("yCoordinate")) {
        return (int) bukkitPlayer.getLocation().getY() + ""; 
      }

      if (params.equalsIgnoreCase("zCoordinate")) {
        return (int) bukkitPlayer.getLocation().getZ() + ""; 
=======
      PlayerManager manager = gm.getPlayerManager();
      Player bukkitPlayer = player.getPlayer();
      UhcPlayer uhcPlayer = manager.getUhcPlayer(bukkitPlayer);
		 	ScoreboardManager scoreboardManager = gm.getScoreboardManager();

      if (params.equalsIgnoreCase("playerAlive")) {
				String substr1 = params.substring(params.indexOf("playerAlive"));
        return scoreboardManager.translatePlaceholders("%" + substr1 + "%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("teamAlive")){
        return scoreboardManager.translatePlaceholders("%teamAlive%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("time")) {
        return scoreboardManager.translatePlaceholders("%time%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("deathmatch")) {
        return scoreboardManager.translatePlaceholders("%deathmatch%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("xCoordinate")) {
        return scoreboardManager.translatePlaceholders("%xCoordinate%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("yCoordinate")) {
				return scoreboardManager.translatePlaceholders("%yCoordinate%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("zCoordinate")) {
				return scoreboardManager.translatePlaceholders("%zCoordinate%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
>>>>>>> Stashed changes:src/main/java/com/gmail/val59000mc/Placeholderapi.java
      }
      
      if (params.equalsIgnoreCase("border")) {
<<<<<<< Updated upstream:src/main/java/com/gmail/val59000mc/placeholderapi.java
        int size = (int) bukkitPlayer.getWorld().getWorldBorder().getSize() / 2;
			  String borderString = "+" + size + " -" + size;

        int playerX = bukkitPlayer.getLocation().getBlockX();
        int playerZ = bukkitPlayer.getLocation().getBlockZ();

        if (playerX < 0) playerX *= -1;
        if (playerZ < 0) playerZ *= -1;

        int distanceX = size - playerX;
        int distanceZ = size - playerZ;

        if (distanceX <= 5 || distanceZ <= 5){
          borderString = ChatColor.RED + borderString;
        }else if (distanceX <= 50 || distanceZ <= 50){
          borderString = ChatColor.YELLOW + borderString;
        }else {
          borderString = ChatColor.GREEN + borderString;
        }

        return borderString; 
      }

      if (params.equalsIgnoreCase("teamColor")) {
        return uhcPlayer.getTeam().getPrefix(); 
      }

      if (params.equalsIgnoreCase("teamKills")) {
        return String.valueOf(uhcPlayer.getTeam().getKills());  
      }

      if (params.equalsIgnoreCase("kills")) {
        return String.valueOf(uhcPlayer.getKills());  
      }

      if (params.equalsIgnoreCase("scenarios")) {
        return gm.getScenarioManager().getEnabledScenarios().toString();
=======
        return scoreboardManager.translatePlaceholders("%border%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("teamColor")) {
				return scoreboardManager.translatePlaceholders("%teamColor%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("teamKills")) {
        return scoreboardManager.translatePlaceholders("%teamKills%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("kills")) {
        return scoreboardManager.translatePlaceholders("%kills%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("scenarios")) {
        return scoreboardManager.translatePlaceholders("%scenarios%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
>>>>>>> Stashed changes:src/main/java/com/gmail/val59000mc/Placeholderapi.java
      }

      if (params.equalsIgnoreCase("kit")) {
        return scoreboardManager.translatePlaceholders("%kit%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("needed")) {
        return scoreboardManager.translatePlaceholders("%needed%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      if (params.equalsIgnoreCase("online")) {
				return scoreboardManager.translatePlaceholders("%online%", uhcPlayer, bukkitPlayer, ScoreboardType.WAITING);
      }

      return null;
    }
}